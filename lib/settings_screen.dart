import 'dart:convert';

import 'package:app/zones.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _ipController = TextEditingController();
  TimeOfDay _alarm;
  String _tz; // timezone value
  String _tzName; // timezone name used to display in the app
  bool _loading = false;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String ip = prefs.getString('ip');
    if (ip != null && ip.isNotEmpty && _ipController.text.isEmpty) {
      setState(() {
        _ipController.text = ip;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                controller: _ipController,
                decoration: InputDecoration(
                  labelText: 'IP',
                ),
              ),
              RaisedButton(
                onPressed: () async {
                  setState(() {
                    _loading = true;
                  });
                  var response =
                      await http.get('http://${_ipController.text}/state');
                  if (response.body.isNotEmpty) {
                    final data = json.decode(response.body);
                    final alarmMinute = data['alarm_minute'];
                    final tz = data['tz'];
                    final tzName = zones.keys
                        .firstWhere((k) => zones[k] == tz, orElse: () => null);
                    setState(() {
                      _alarm = TimeOfDay(
                        hour: alarmMinute ~/ 60,
                        minute: alarmMinute % 60,
                      );
                      _tz = tz;
                      _tzName = tzName; // multiple names can have same value!
                    });
                  }
                  setState(() {
                    _loading = false;
                  });
                },
                child: Text('Get state'),
              ),
              SizedBox(height: 10),
              Divider(thickness: 10),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Alarm time: ${_alarm?.hour}:${_alarm?.minute}'),
                  RaisedButton(
                    onPressed: () async {
                      final selected = await showTimePicker(
                          context: context, initialTime: _alarm);
                      setState(() {
                        _alarm = selected;
                      });
                    },
                    child: Text('Change'),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Timezone:'),
                  DropdownButton(
                    value: _tzName,
                    items: zones.keys
                        .map(
                          (k) => DropdownMenuItem<String>(
                            value: k,
                            child: Text(k),
                          ),
                        )
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        _tzName = value;
                        _tz = zones[value];
                      });
                    },
                  ),
                ],
              ),
              RaisedButton(
                onPressed: () async {
                  setState(() {
                    _loading = true;
                  });
                  final data = {};
                  if (_alarm != null) {
                    final alarmMinute = _alarm.hour * 60 + _alarm.minute;
                    data['alarm_minute'] = alarmMinute;
                  }
                  if (_tz != null) {
                    data['tz'] = _tz;
                  }
                  final body = json.encode(data);
                  final response = await http.post(
                    'http://${_ipController.text}/state',
                    body: body,
                  );
                  print(response.statusCode);
                  setState(() {
                    _loading = false;
                  });
                },
                child: Text('SAVE'),
              ),
            ],
          ),
        ),
        Container(
          child: _loading
              ? Center(child: CircularProgressIndicator())
              : Container(),
        ),
      ],
    );
  }
}
