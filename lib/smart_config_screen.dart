import 'dart:async';

import 'package:flutter/material.dart';

import 'package:esptouch_flutter/esptouch_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SmartConfigScreen extends StatefulWidget {
  @override
  _SmartConfigScreenState createState() => _SmartConfigScreenState();
}

class _SmartConfigScreenState extends State<SmartConfigScreen> {
  GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController _ssidController = TextEditingController();
  TextEditingController _bSsidController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  ESPTouchTask _espTouchTask;
  Stream<ESPTouchResult> _espTouchResultStream;
  StreamSubscription<ESPTouchResult> _espTouchResultStreamSubscription;

  @override
  void initState() {
    super.initState();
    // todo this is my personal info, remove it
    _ssidController.text = 'TurkTelekom_T070A';
    _bSsidController.text = 'C4:71:54:C1:07:0A';
    _passwordController.text = 'fawg4FxJ';
  }

  @override
  void dispose() {
    super.dispose();
    _ssidController.dispose();
    _bSsidController.dispose();
    _passwordController.dispose();
    _espTouchResultStreamSubscription?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _espTouchResultStream == null ? _form() : _result(),
    );
  }

  Widget _form() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          TextFormField(
            controller: _ssidController,
            decoration: InputDecoration(labelText: 'SSID'),
            validator: (value) {
              if (value.isEmpty) {
                return 'This field is required';
              } else {
                return null;
              }
            },
          ),
          TextFormField(
            controller: _bSsidController,
            decoration: InputDecoration(labelText: 'BSSID'),
            validator: (value) {
              if (value.isEmpty) {
                return 'This field is required';
              } else {
                return null;
              }
            },
          ),
          TextFormField(
            controller: _passwordController,
            decoration: InputDecoration(labelText: 'Password'),
          ),
          RaisedButton(
            onPressed: () async {
              if (!_formKey.currentState.validate()) {
                return;
              } else {
                _espTouchTask = new ESPTouchTask(
                  ssid: _ssidController.text,
                  bssid: _bSsidController.text,
                  password: _passwordController.text,
                );
                setState(() {});
                _espTouchResultStream = _espTouchTask.execute();
                await _espTouchResultStreamSubscription?.cancel();
                _espTouchResultStreamSubscription =
                    _espTouchResultStream.listen((result) async {
                  if (result?.ip != null) {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    await prefs.setString('ip', result.ip);
                  }
                });
                setState(() {});
              }
            },
            child: Text('CONNECT'),
          ),
        ],
      ),
    );
  }

  Widget _result() {
    return StreamBuilder<ESPTouchResult>(
      stream: _espTouchResultStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 100),
                CircularProgressIndicator(),
                SizedBox(height: 50),
                RaisedButton(
                  onPressed: () async {
                    await _espTouchResultStreamSubscription?.cancel();
                    _espTouchResultStreamSubscription = null;
                    _espTouchResultStream = null;
                    setState(() {});
                  },
                  child: Text('CANCEL'),
                ),
                SizedBox(height: 100),
              ],
            ),
          );
        } else {
          return Container(
            child: Center(
              child: Text('CONNECTED\nIoT IP: ${snapshot.data.ip}'),
            ),
          );
        }
      },
    );
  }
}
